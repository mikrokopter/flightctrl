// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) 04.2007 Holger Buss
// + Nur f�r den privaten Gebrauch
// + www.MikroKopter.com
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Es gilt f�r das gesamte Projekt (Hardware, Software, Bin�rfiles, Sourcecode und Dokumentation),
// + dass eine Nutzung (auch auszugsweise) nur f�r den privaten und nicht-kommerziellen Gebrauch zul�ssig ist.
// + Sollten direkte oder indirekte kommerzielle Absichten verfolgt werden, ist mit uns (info@mikrokopter.de) Kontakt
// + bzgl. der Nutzungsbedingungen aufzunehmen.
// + Eine kommerzielle Nutzung ist z.B.Verkauf von MikroKoptern, Best�ckung und Verkauf von Platinen oder Baus�tzen,
// + Verkauf von Luftbildaufnahmen, usw.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Werden Teile des Quellcodes (mit oder ohne Modifikation) weiterverwendet oder ver�ffentlicht,
// + unterliegen sie auch diesen Nutzungsbedingungen und diese Nutzungsbedingungen incl. Copyright m�ssen dann beiliegen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Sollte die Software (auch auszugesweise) oder sonstige Informationen des MikroKopter-Projekts
// + auf anderen Webseiten oder Medien ver�ffentlicht werden, muss unsere Webseite "http://www.mikrokopter.de"
// + eindeutig als Ursprung verlinkt und genannt werden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Keine Gew�hr auf Fehlerfreiheit, Vollst�ndigkeit oder Funktion
// + Benutzung auf eigene Gefahr
// + Wir �bernehmen keinerlei Haftung f�r direkte oder indirekte Personen- oder Sachsch�den
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Portierung der Software (oder Teile davon) auf andere Systeme (ausser der Hardware von www.mikrokopter.de) ist nur
// + mit unserer Zustimmung zul�ssig
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Funktion printf_P() unterliegt ihrer eigenen Lizenz und ist hiervon nicht betroffen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Redistributions of source code (with or without modifications) must retain the above copyright notice,
// + this list of conditions and the following disclaimer.
// +   * Neither the name of the copyright holders nor the names of contributors may be used to endorse or promote products derived
// +     from this software without specific prior written permission.
// +   * The use of this project (hardware, software, binary files, sources and documentation) is only permittet
// +     for non-commercial use (directly or indirectly)
// +     Commercial use (for excample: selling of MikroKopters, selling of PCBs, assembly, ...) is only permitted
// +     with our written permission
// +   * If sources or documentations are redistributet on other webpages, out webpage (http://www.MikroKopter.de) must be
// +     clearly linked as origin
// +   * porting to systems other than hardware from www.mikrokopter.de is not allowed
// +  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// +  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// +  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// +  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// +  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// +  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// +  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// +  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// +  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// +  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// +  POSSIBILITY OF SUCH DAMAGE.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "main.h"

unsigned char EEPromArray[E2END+1] EEMEM;
unsigned char PlatinenVersion = 10;
unsigned char SendVersionToNavi = 1;
// -- Parametersatz aus EEPROM lesen ---
// number [1..5]
void ReadParameterSet(unsigned char number, unsigned char *buffer, unsigned char length)
{
   if((number > 5)||(number < 1)) number = 3;
   eeprom_read_block(buffer, &EEPromArray[EEPROM_ADR_PARAM_BEGIN + length * (number - 1)], length);
   LED_Init();
}

// -- Parametersatz ins EEPROM schreiben ---
// number [1..5]
void WriteParameterSet(unsigned char number, unsigned char *buffer, unsigned char length)
{
   if(number > 5) number = 5;
   if(number < 1) return;
   eeprom_write_block(buffer, &EEPromArray[EEPROM_ADR_PARAM_BEGIN + length * (number - 1)], length);
   eeprom_write_byte(&EEPromArray[EEPROM_ADR_PARAM_LENGTH], length); // L�nge der Datens�tze merken
   eeprom_write_block(buffer, &EEPromArray[EEPROM_ADR_CHANNELS], 8); // 8 Kan�le merken
   SetActiveParamSetNumber(number);
   LED_Init();
}

unsigned char GetActiveParamSetNumber(void)
{
	unsigned char set;
	set = eeprom_read_byte(&EEPromArray[EEPROM_ADR_ACTIVE_SET]);
	if((set > 5) || (set < 1))
	{
		set = 3;
		SetActiveParamSetNumber(set);  				// diesen Parametersatz als aktuell merken
	}
	return(set);
}


void SetActiveParamSetNumber(unsigned char number)
{
	if(number > 5) number = 5;
   	if(number < 1) return;
   	eeprom_write_byte(&EEPromArray[EEPROM_ADR_ACTIVE_SET], number);  				// diesen Parametersatz als aktuell merken
}


void CalMk3Mag(void)
{
 static unsigned char stick = 1;

 if(PPM_in[EE_Parameter.Kanalbelegung[K_NICK]] > -20) stick = 0;
 if((PPM_in[EE_Parameter.Kanalbelegung[K_NICK]] < -70) && !stick)
  {
   stick = 1;
   WinkelOut.CalcState++;
   if(WinkelOut.CalcState > 4)
    {
//     WinkelOut.CalcState = 0; // in Uart.c
     beeptime = 1000;
    }
   else Piep(WinkelOut.CalcState);
  }
  DebugOut.Analog[19] = WinkelOut.CalcState;
}

//############################################################################
//Hauptprogramm
int main (void)
//############################################################################
{
	unsigned int timer,i;
    DDRB  = 0x00;
    PORTB = 0x00;
    for(timer = 0; timer < 1000; timer++); // verz�gern
    if(PINB & 0x01)
     {
      if(PINB & 0x02) PlatinenVersion = 13;
       else           PlatinenVersion = 11;
     }
    else
     {
      if(PINB & 0x02) PlatinenVersion = 20;
       else           PlatinenVersion = 10;
     }

    DDRC  = 0x81; // SCL
    DDRC  |=0x40; // HEF4017 Reset
    PORTC = 0xff; // Pullup SDA
    DDRB  = 0x1B; // LEDs und Druckoffset
    PORTB = 0x01; // LED_Rot
    DDRD  = 0x3E; // Speaker & TXD & J3 J4 J5
    DDRD  |=0x80; // J7 -> Servo signal
	PORTD = 0x47; // LED
    HEF4017R_ON;
    MCUSR &=~(1<<WDRF);
    WDTCSR |= (1<<WDCE)|(1<<WDE);
    WDTCSR = 0;

    beeptime = 2000;

	StickGier = 0; PPM_in[K_GAS] = 0;StickRoll = 0; StickNick = 0;
    if(PlatinenVersion >= 20)  GIER_GRAD_FAKTOR = 1160; else GIER_GRAD_FAKTOR = 1291; // unterschiedlich f�r ME und ENC
    ROT_OFF;

    Timer_Init();
	TIMER2_Init();
	UART_Init();
    rc_sum_init();
   	ADC_Init();
	i2c_init();
	SPI_MasterInit();

	sei();

	printf("\n\r===================================");
	printf("\n\rFlightControl\n\rHardware:%d.%d\n\rSoftware:V%d.%d%c ",PlatinenVersion/10,PlatinenVersion%10, VERSION_MAJOR, VERSION_MINOR,VERSION_PATCH + 'a');
    if(UCSR1A == 0x20 && UCSR1C == 0x06)  // initial Values for 644P
     { 
      Uart1Init();
     }
	GRN_ON;
	ReadParameterSet(3, (unsigned char *) &EE_Parameter.Kanalbelegung[0], 9); // read only the first bytes

    if((eeprom_read_byte(&EEPromArray[EEPROM_ADR_MIXER_TABLE]) == MIXER_REVISION) && // Check Revision in the first Byte
       (eeprom_read_byte(&EEPromArray[EEPROM_ADR_VALID]) != 0xff))                   // Settings reset via Koptertool
	{
     unsigned char i;     
	 RequiredMotors = 0;
     eeprom_read_block(&Mixer, &EEPromArray[EEPROM_ADR_MIXER_TABLE], sizeof(Mixer));
     for(i=0; i<16;i++) { if(Mixer.Motor[i][0] > 0) RequiredMotors++;}
	} 
	else // default
	{
     unsigned char i;     
	 printf("\n\rGenerating default Mixer Table");
	 for(i=0; i<16;i++) { Mixer.Motor[i][0] = 0;Mixer.Motor[i][1] = 0;Mixer.Motor[i][2] = 0;Mixer.Motor[i][3] = 0;};
     // default = Quadro
     Mixer.Motor[0][0] = 64; Mixer.Motor[0][1] = +64; Mixer.Motor[0][2] =   0; Mixer.Motor[0][3] = +64;
     Mixer.Motor[1][0] = 64; Mixer.Motor[1][1] = -64; Mixer.Motor[1][2] =   0; Mixer.Motor[1][3] = +64;
     Mixer.Motor[2][0] = 64; Mixer.Motor[2][1] =   0; Mixer.Motor[2][2] = -64; Mixer.Motor[2][3] = -64;
     Mixer.Motor[3][0] = 64; Mixer.Motor[3][1] =   0; Mixer.Motor[3][2] = +64; Mixer.Motor[3][3] = -64;
	 Mixer.Revision = MIXER_REVISION;
     memcpy(Mixer.Name, "Quadro\0", 11);   
     eeprom_write_block(&Mixer, &EEPromArray[EEPROM_ADR_MIXER_TABLE], sizeof(Mixer));
    }
    printf("\n\rMixer-Config: '%s' (%u Motors)",Mixer.Name,RequiredMotors);
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Check connected BL-Ctrls
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	printf("\n\rFound BL-Ctrl: ");
    motorread = 0;   UpdateMotor = 0;   SendMotorData(); while(!UpdateMotor); motorread = 0;  // read the first I2C-Data
	for(i=0; i < MAX_MOTORS; i++)
	 {
	  UpdateMotor = 0;
      SendMotorData();
	  while(!UpdateMotor);
	  if(MotorPresent[i]) printf("%d ",i+1);
     }
	for(i=0; i < MAX_MOTORS; i++)
	 {
	  if(!MotorPresent[i] && Mixer.Motor[i][0] > 0) printf("\n\r\n\r!! MISSING BL-CTRL: %d !!",i+1);
	  MotorError[i] = 0;
     }
	printf("\n\r===================================");
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Check Settings
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if(eeprom_read_byte(&EEPromArray[EEPROM_ADR_VALID]) != EE_DATENREVISION)
	{
	  DefaultKonstanten1();
	  printf("\n\rInit. EEPROM");
	  for (unsigned char i=1;i<6;i++)
      {
       if(i==2) DefaultKonstanten2(); // Kamera
       if(i==3) DefaultKonstanten3(); // Beginner
       if(i>3)  DefaultKonstanten2(); // Kamera
	   if(PlatinenVersion >= 20) 
	    { 
		 EE_Parameter.Gyro_D = 5;
		 EE_Parameter.Driftkomp = 0;
		 EE_Parameter.GyroAccFaktor = 27;
         EE_Parameter.WinkelUmschlagNick = 78;
         EE_Parameter.WinkelUmschlagRoll = 78;
		}
    // valid Stick-Settings?
  	   if(eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS]) < 12 && eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+1]) < 12 && eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+2]) < 12 && eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+3]) < 12)
	    {
         EE_Parameter.Kanalbelegung[0] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+0]);
         EE_Parameter.Kanalbelegung[1] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+1]);
         EE_Parameter.Kanalbelegung[2] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+2]);
         EE_Parameter.Kanalbelegung[3] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+3]);
         EE_Parameter.Kanalbelegung[4] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+4]);
         EE_Parameter.Kanalbelegung[5] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+5]);
         EE_Parameter.Kanalbelegung[6] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+6]);
         EE_Parameter.Kanalbelegung[7] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+7]);
         if(i==1) printf(": Generating Default-Parameter using old Stick Settings");
		} else DefaultStickMapping();
       WriteParameterSet(i, (unsigned char *) &EE_Parameter.Kanalbelegung[0], STRUCT_PARAM_LAENGE);
      }
	  SetActiveParamSetNumber(3); // default-Setting
	  eeprom_write_byte(&EEPromArray[EEPROM_ADR_VALID], EE_DATENREVISION);
	}

    if(eeprom_read_byte(&EEPromArray[EEPROM_ADR_ACC_NICK]) > 4)
     {
       printf("\n\rACC not calibrated !");
     }

	ReadParameterSet(GetActiveParamSetNumber(), (unsigned char *) &EE_Parameter.Kanalbelegung[0], STRUCT_PARAM_LAENGE);
    printf("\n\rUsing parameterset %d", GetActiveParamSetNumber());


	if(EE_Parameter.GlobalConfig & CFG_HOEHENREGELUNG)
	 {
	   printf("\n\rCalibrating pressure sensor..");
	   timer = SetDelay(1000);
       SucheLuftruckOffset();
   	   while (!CheckDelay(timer));
       printf("OK\n\r");
	}

	SetNeutral();

	ROT_OFF;

    beeptime = 2000;
    ExternControl.Digital[0] = 0x55;


	printf("\n\rControl: ");
	if (EE_Parameter.GlobalConfig & CFG_HEADING_HOLD) printf("HeadingHold");
	else printf("Normal (ACC-Mode)");

	printf("\n\r===================================\n\r");

    LcdClear();
    I2CTimeout = 5000;
    WinkelOut.Orientation = 1;
	while (1)
	{
   	    if(UpdateMotor && AdReady)      // ReglerIntervall
            {
  		    UpdateMotor=0;
            if(WinkelOut.CalcState) CalMk3Mag();
            else MotorRegler();
            SendMotorData();
            ROT_OFF;
            if(PcZugriff) PcZugriff--;
             else
              {
			   ExternControl.Config = 0;
               ExternStickNick = 0;
               ExternStickRoll = 0;
               ExternStickGier = 0;
              }
            if(SenderOkay)  SenderOkay--;
            if(NaviDataOkay) 
			 {
			  if(--NaviDataOkay == 0)
			   {
	            GPS_Nick = 0;
                GPS_Roll = 0;
               }
              }
            if(!--I2CTimeout || MissingMotor)
                {
                  if(!I2CTimeout) 
				   {
				    i2c_reset();
                    I2CTimeout = 5;
					}
                  if((BeepMuster == 0xffff) && MotorenEin)
                   {
                    beeptime = 10000;
                    BeepMuster = 0x0080;
                   }
                }
            else
                {
                 ROT_OFF;
                }
            if(SIO_DEBUG && (!UpdateMotor || !MotorenEin))
              {
               DatenUebertragung();
               BearbeiteRxDaten();
              }
              else BearbeiteRxDaten();
         if(CheckDelay(timer))
            {
            if(UBat < EE_Parameter.UnterspannungsWarnung)
                {
                  if(BeepMuster == 0xffff)
                   {
                    beeptime = 6000;
                    BeepMuster = 0x0300;
                   }
                }
             SPI_StartTransmitPacket();

             SendSPI = 4;
 			 timer = SetDelay(20);
            }
           LED_Update();
          }
     if(!SendSPI) { SPI_TransmitByte(); }
    }
 return (1);
}


//if(HoehenReglerAktiv && NaviDataOkay && SenderOkay < 160 && SenderOkay > 10 && FromNaviCtrl_Value.SerialDataOkay > 220) SenderOkay = 160;
//if(HoehenReglerAktiv && NaviDataOkay && SenderOkay < 101 && SenderOkay > 10 && FromNaviCtrl_Value.SerialDataOkay > 1) SenderOkay = 101;
