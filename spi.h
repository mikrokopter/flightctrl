// ######################## SPI - FlightCtrl ###################
#ifndef _SPI_H
#define _SPI_H

#include <util/delay.h>

#define USE_SPI_COMMUNICATION

#define SPI_PROTOCOL_COMP   1

//-----------------------------------------
#define DDR_SPI DDRB
#define DD_SS   PB4
#define DD_SCK  PB7
#define DD_MOSI PB5
#define DD_MISO PB6

// for compatibility reasons gcc3.x <-> gcc4.x
#ifndef SPCR
#define SPCR   SPCR0
#endif
#ifndef SPE
#define SPE   SPE0
#endif
#ifndef MSTR
#define MSTR   MSTR0
#endif
#ifndef SPR1
#define SPR1   SPR01
#endif
#ifndef SPR0
#define SPR0   SPR00
#endif
#ifndef SPIE
#define SPIE   SPIE0
#endif
#ifndef SPDR
#define SPDR   SPDR0
#endif
#ifndef SPIF
#define SPIF   SPIF0
#endif
#ifndef SPSR
#define SPSR   SPSR0
#endif
// -------------------------

#define SLAVE_SELECT_DDR_PORT   DDRC
#define SLAVE_SELECT_PORT       PORTC
#define SPI_SLAVE_SELECT        PC5


#define SPI_CMD_USER        10
#define SPI_CMD_STICK       11
#define SPI_CMD_MISC        12
#define SPI_CMD_PARAMETER1  13
#define SPI_CMD_VERSION     14

struct str_ToNaviCtrl
{
 unsigned char Sync1, Sync2;
 unsigned char Command;
 signed int  IntegralNick;
 signed int  IntegralRoll;
 signed int  AccNick;
 signed int  AccRoll;
 signed int  GyroCompass;
 signed int  GyroNick;
 signed int  GyroRoll;
 signed int  GyroGier;
 union
 { char Byte[12];
   int  Int[6];
   long Long[3];
   float Float[3];
 } Param;
 unsigned char Chksum;
};

#define SPI_KALMAN           103

struct str_FromNaviCtrl
{
 unsigned char Command;
  signed int  GPS_Nick;
  signed int  GPS_Roll;
  signed int  GPS_Gier;
  signed int  CompassValue;
  signed int  Status;
  unsigned int BeepTime;
  union
  { char Byte[12];
    int  Int[6];
    long Long[3];
    float Float[3];
  } Param;
  unsigned char Chksum;
};

struct str_FromNaviCtrl_Value
{
 signed char Kalman_K;
 signed char Kalman_MaxDrift;
 signed char Kalman_MaxFusion;
 unsigned char SerialDataOkay;
};

struct str_SPI_VersionInfo
{
  unsigned char Major;
  unsigned char Minor;
  unsigned char Patch;
  unsigned char Compatible;
};

#ifdef USE_SPI_COMMUNICATION

extern struct str_FromNaviCtrl_Value FromNaviCtrl_Value;
extern struct str_ToNaviCtrl   ToNaviCtrl;
extern struct str_FromNaviCtrl FromNaviCtrl;
extern unsigned char SPI_CommandCounter,NaviDataOkay;

//#define SPI_CMD_VALUE   0x03

extern void SPI_MasterInit(void);
extern void SPI_StartTransmitPacket(void);
extern void UpdateSPI_Buffer(void);
extern void SPI_TransmitByte(void);
#else


// -------------------------------- Dummy -----------------------------------------
#define  SPI_MasterInit() ;
#define  SPI_StartTransmitPacket() ;
#define  UpdateSPI_Buffer() ;
#define  SPI_TransmitByte() ;
#endif


#endif
