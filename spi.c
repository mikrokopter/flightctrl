// ######################## SPI - FlightCtrl ###################
#include "main.h"


//struct str_ToNaviCtrl_Version   ToNaviCtrl_Version;
//struct str_FromNaviCtrl_Version   FromNaviCtrl_Version;
struct str_ToNaviCtrl   ToNaviCtrl;
struct str_FromNaviCtrl   FromNaviCtrl;
struct str_FromNaviCtrl_Value FromNaviCtrl_Value;
struct str_SPI_VersionInfo SPI_VersionInfo;

unsigned char              SPI_BufferIndex;
unsigned char              SPI_RxBufferIndex;

volatile unsigned char     SPI_Buffer[sizeof(FromNaviCtrl)];
unsigned char *SPI_TX_Buffer;

unsigned char SPITransferCompleted, SPI_ChkSum;
unsigned char SPI_RxDataValid,NaviDataOkay = 0;

unsigned char SPI_CommandSequence[] = { SPI_CMD_USER, SPI_CMD_STICK, SPI_CMD_PARAMETER1, SPI_CMD_STICK, SPI_CMD_MISC, SPI_CMD_VERSION };
unsigned char SPI_CommandCounter = 0;

#ifdef USE_SPI_COMMUNICATION

//------------------------------------------------------
void SPI_MasterInit(void)
{
  DDR_SPI |= (1<<DD_MOSI)|(1<<DD_SCK);    // Set MOSI and SCK output, all others input
  SLAVE_SELECT_DDR_PORT |= (1 << SPI_SLAVE_SELECT);

  SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR1)|(0<<SPR0)|(0<<SPIE);   // Enable SPI, Master, set clock rate fck/64
  SPSR = 0;//(1<<SPI2X);

  SLAVE_SELECT_PORT |=  (1 << SPI_SLAVE_SELECT);
  SPITransferCompleted = 1;

  //SPDR = 0x00;  // dummy write

  ToNaviCtrl.Sync1 = 0xAA;
  ToNaviCtrl.Sync2 = 0x83;

  ToNaviCtrl.Command = SPI_CMD_USER;
  ToNaviCtrl.IntegralNick = 0;
  ToNaviCtrl.IntegralRoll = 0;
  FromNaviCtrl_Value.SerialDataOkay = 0;
  SPI_RxDataValid = 0;

  SPI_VersionInfo.Major = VERSION_MAJOR;
  SPI_VersionInfo.Minor = VERSION_MINOR;
  SPI_VersionInfo.Patch = VERSION_PATCH;
  SPI_VersionInfo.Compatible = NC_SPI_COMPATIBLE;
}

//------------------------------------------------------
void SPI_StartTransmitPacket(void)
{
   //if ((SLAVE_SELECT_PORT & (1 << SPI_SLAVE_SELECT)) == 0) return;    // transfer of prev. packet not completed
   if (!SPITransferCompleted) return;
//   _delay_us(30);

   SLAVE_SELECT_PORT &=  ~(1 << SPI_SLAVE_SELECT);  // SelectSlave
   SPI_TX_Buffer = (unsigned char *) &ToNaviCtrl;

   ToNaviCtrl.Command = SPI_CommandSequence[SPI_CommandCounter++];
   if (SPI_CommandCounter >= sizeof(SPI_CommandSequence)) SPI_CommandCounter = 0;

   SPITransferCompleted = 0;
   UpdateSPI_Buffer();                              // update buffer

   SPI_BufferIndex = 1;
  //ebugOut.Analog[16]++;
   // -- Debug-Output ---
   //----
   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); 	   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop");
   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); 	   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop");
   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); 	   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop");
   ToNaviCtrl.Chksum = ToNaviCtrl.Sync1;
   SPDR = ToNaviCtrl.Sync1;                  // Start transmission
//     SLAVE_SELECT_PORT |=  (1 << SPI_SLAVE_SELECT);   // DeselectSlave

}

//------------------------------------------------------
//SIGNAL(SIG_SPI)
void SPI_TransmitByte(void)
{
   static unsigned char SPI_RXState = 0;
   unsigned char rxdata;
   static unsigned char rxchksum;

   if (SPITransferCompleted) return;
   if (!(SPSR & (1 << SPIF))) return;
  SendSPI = 4;

//   _delay_us(30);
  SLAVE_SELECT_PORT |=  (1 << SPI_SLAVE_SELECT);   // DeselectSlave

  rxdata = SPDR;
  switch ( SPI_RXState)
  {
  case 0:

			SPI_RxBufferIndex = 0;
			//DebugOut.Analog[17]++;
			rxchksum = rxdata;
			if (rxdata == 0x81 )  { SPI_RXState  = 1;  }   // 1. Syncbyte ok

   	   break;

   case 1:
 		    if (rxdata == 0x55) { rxchksum += rxdata; SPI_RXState  = 2;  }   // 2. Syncbyte ok
	         else SPI_RXState  = 0;
			//DebugOut.Analog[18]++;
   	   break;

   case 2:
		   SPI_Buffer[SPI_RxBufferIndex++]= rxdata;             // get data
           //DebugOut.Analog[19]++;
           if (SPI_RxBufferIndex >= sizeof(FromNaviCtrl))
   		   {

      		if (rxdata == rxchksum)
			{
	          unsigned char *ptr = (unsigned char *)&FromNaviCtrl;

   			  memcpy(ptr, (unsigned char *) SPI_Buffer,  sizeof(SPI_Buffer));

			  SPI_RxDataValid = 1;
			}
			else SPI_RxDataValid = 0;

			SPI_RXState  = 0;
   		   }
		  else rxchksum += rxdata;
	break;

  }

   if (SPI_BufferIndex < sizeof(ToNaviCtrl))
     {
 	   SLAVE_SELECT_PORT &=  ~(1 << SPI_SLAVE_SELECT);  // SelectSlave
 	   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); 	   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop");
	   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); 	   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop");
	   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop");	   asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop"); asm volatile ("nop");

	   SPDR = SPI_TX_Buffer[SPI_BufferIndex];
	   ToNaviCtrl.Chksum += SPI_TX_Buffer[SPI_BufferIndex];
	//   SLAVE_SELECT_PORT |=  (1 << SPI_SLAVE_SELECT);   // DeselectSlave

 	 }
	 else SPITransferCompleted = 1;

	 SPI_BufferIndex++;
}


//------------------------------------------------------
void UpdateSPI_Buffer(void)
{
  signed int tmp;
  cli();

  ToNaviCtrl.IntegralNick = (int) (IntegralNick / (long)(EE_Parameter.GyroAccFaktor * 4));
  ToNaviCtrl.IntegralRoll = (int) (IntegralRoll / (long)(EE_Parameter.GyroAccFaktor * 4));
  ToNaviCtrl.GyroCompass = (10 * ErsatzKompass) / GIER_GRAD_FAKTOR;
  ToNaviCtrl.AccNick = ((int) ACC_AMPLIFY * (NaviAccNick / NaviCntAcc))/4;
  ToNaviCtrl.AccRoll = ((int) ACC_AMPLIFY * (NaviAccRoll / NaviCntAcc))/4;
  NaviCntAcc = 0; NaviAccNick = 0; NaviAccRoll = 0;
//  ToNaviCtrl.User8 = Parameter_UserParam8;
//  ToNaviCtrl.CalState = WinkelOut.CalcState;

   switch(ToNaviCtrl.Command)  //
   {
	 case SPI_CMD_USER:
				ToNaviCtrl.Param.Byte[0] = Parameter_UserParam1;
				ToNaviCtrl.Param.Byte[1] = Parameter_UserParam2;
				ToNaviCtrl.Param.Byte[2] = Parameter_UserParam3;
				ToNaviCtrl.Param.Byte[3] = Parameter_UserParam4;
				ToNaviCtrl.Param.Byte[4] = Parameter_UserParam5;
				ToNaviCtrl.Param.Byte[5] = Parameter_UserParam6;
				ToNaviCtrl.Param.Byte[6] = Parameter_UserParam7;
				ToNaviCtrl.Param.Byte[7] = Parameter_UserParam8;
				ToNaviCtrl.Param.Byte[8] = (unsigned char) MikroKopterFlags;
                MikroKopterFlags &= ~(FLAG_CALIBRATE | FLAG_START);
	            ToNaviCtrl.Param.Byte[9] = (unsigned char) UBat;
	            ToNaviCtrl.Param.Byte[10] =(unsigned char) EE_Parameter.UnterspannungsWarnung;
	            ToNaviCtrl.Param.Byte[11] =(unsigned char) eeprom_read_byte(&EEPromArray[EEPROM_ADR_ACTIVE_SET]);
        break;
	 case SPI_CMD_PARAMETER1:
				ToNaviCtrl.Param.Byte[0] = EE_Parameter.NaviGpsModeControl;     // Parameters for the Naviboard
				ToNaviCtrl.Param.Byte[1] = EE_Parameter.NaviGpsGain;
				ToNaviCtrl.Param.Byte[2] = EE_Parameter.NaviGpsP;
				ToNaviCtrl.Param.Byte[3] = EE_Parameter.NaviGpsI;
				ToNaviCtrl.Param.Byte[4] = EE_Parameter.NaviGpsD;
				ToNaviCtrl.Param.Byte[5] = EE_Parameter.NaviGpsACC;
				ToNaviCtrl.Param.Byte[6] = EE_Parameter.NaviGpsMinSat;
                ToNaviCtrl.Param.Byte[7] = EE_Parameter.NaviStickThreshold;
                ToNaviCtrl.Param.Byte[8] = EE_Parameter.NaviOperatingRadius;
                ToNaviCtrl.Param.Byte[9] = EE_Parameter.NaviWindCorrection;
                ToNaviCtrl.Param.Byte[10] = EE_Parameter.NaviSpeedCompensation;
				ToNaviCtrl.Param.Byte[11] = EE_Parameter.NaviAngleLimitation;
	    break;
	 case SPI_CMD_STICK:
                tmp = PPM_in[EE_Parameter.Kanalbelegung[K_GAS]];  if(tmp > 127) tmp = 127; else if(tmp < -127) tmp = -127;
				ToNaviCtrl.Param.Byte[0] = (char) tmp;
                tmp = PPM_in[EE_Parameter.Kanalbelegung[K_GIER]]; if(tmp > 127) tmp = 127; else if(tmp < -127) tmp = -127;
				ToNaviCtrl.Param.Byte[1] = (char) tmp;
                tmp = PPM_in[EE_Parameter.Kanalbelegung[K_ROLL]]; if(tmp > 127) tmp = 127; else if(tmp < -127) tmp = -127;
				ToNaviCtrl.Param.Byte[2] = (char) tmp;
                tmp = PPM_in[EE_Parameter.Kanalbelegung[K_NICK]]; if(tmp > 127) tmp = 127; else if(tmp < -127) tmp = -127;
				ToNaviCtrl.Param.Byte[3] = (char) tmp;
				ToNaviCtrl.Param.Byte[4] = (unsigned char) Poti1;
				ToNaviCtrl.Param.Byte[5] = (unsigned char) Poti2;
				ToNaviCtrl.Param.Byte[6] = (unsigned char) Poti3;
	            ToNaviCtrl.Param.Byte[7] = (unsigned char) Poti4;
	            ToNaviCtrl.Param.Byte[8] = (unsigned char) SenderOkay;
	    break;
	 case SPI_CMD_MISC:
                if(WinkelOut.CalcState > 5)
                  {
                    WinkelOut.CalcState = 0;
                    ToNaviCtrl.Param.Byte[0] = 5;
                  }
				else ToNaviCtrl.Param.Byte[0] = WinkelOut.CalcState;
				ToNaviCtrl.Param.Int[1] = HoehenWert;
				ToNaviCtrl.Param.Byte[1] = EE_Parameter.NaviPH_LoginTime;
				ToNaviCtrl.Param.Byte[4] = EE_Parameter.NaviGpsPLimit;
				ToNaviCtrl.Param.Byte[5] = EE_Parameter.NaviGpsILimit;
				ToNaviCtrl.Param.Byte[6] = EE_Parameter.NaviGpsDLimit;
	    break;

	 case SPI_CMD_VERSION:
				ToNaviCtrl.Param.Byte[0] = SPI_VersionInfo.Major;
				ToNaviCtrl.Param.Byte[1] = SPI_VersionInfo.Minor;
				ToNaviCtrl.Param.Byte[2] = SPI_VersionInfo.Patch;
				ToNaviCtrl.Param.Byte[3] = SPI_VersionInfo.Compatible;
				ToNaviCtrl.Param.Byte[4] = PlatinenVersion;
 	    break;
   }

  sei();

  if(SPI_RxDataValid)
  {
   if(abs(FromNaviCtrl.GPS_Nick) < 512 && abs(FromNaviCtrl.GPS_Roll) < 512 && (EE_Parameter.GlobalConfig & CFG_GPS_AKTIV))
   {
    GPS_Nick = FromNaviCtrl.GPS_Nick;
    GPS_Roll = FromNaviCtrl.GPS_Roll;
	NaviDataOkay = 250;
   }
    if(FromNaviCtrl.CompassValue <= 360)   KompassValue = FromNaviCtrl.CompassValue;
    KompassRichtung = ((540 + KompassValue - KompassStartwert) % 360) - 180;

    if(FromNaviCtrl.BeepTime > beeptime && !WinkelOut.CalcState) beeptime = FromNaviCtrl.BeepTime;

	  switch (FromNaviCtrl.Command)
	  {
	    case  SPI_KALMAN:
               FromNaviCtrl_Value.Kalman_K = FromNaviCtrl.Param.Byte[0];
               FromNaviCtrl_Value.Kalman_MaxFusion = FromNaviCtrl.Param.Byte[1];
               FromNaviCtrl_Value.Kalman_MaxDrift = FromNaviCtrl.Param.Byte[2];
               FromNaviCtrl_Value.SerialDataOkay = FromNaviCtrl.Param.Byte[3];
               break;

		default:
		  break;
	  }
  }
  else
  {
//    KompassValue = 0;
//    KompassRichtung = 0;
	GPS_Nick = 0;
    GPS_Roll = 0;
  }
}

#endif


