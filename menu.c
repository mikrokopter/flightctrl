// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) 04.2007 Holger Buss
// + only for non-profit use
// + www.MikroKopter.com
// + see the File "License.txt" for further Informations
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "main.h"

unsigned int  TestInt = 0;
#define ARRAYGROESSE 10
unsigned char Array[ARRAYGROESSE] = {1,2,3,4,5,6,7,8,9,10};
char DisplayBuff[80] = "Hallo Welt";
unsigned char DispPtr = 0;

unsigned char MaxMenue = 13;
unsigned char MenuePunkt = 0;
unsigned char RemoteKeys = 0;

#define KEY1    0x01
#define KEY2    0x02
#define KEY3    0x04
#define KEY4    0x08
#define KEY5    0x10

void LcdClear(void)
{
 unsigned char i;
 for(i=0;i<80;i++) DisplayBuff[i] = ' ';
}

void Menu(void)
 {

  if(MenuePunkt > MaxMenue) MenuePunkt = MaxMenue;

  if(RemoteKeys & KEY1) { if(MenuePunkt) MenuePunkt--; else MenuePunkt = MaxMenue;}
  if(RemoteKeys & KEY2) { if(MenuePunkt == MaxMenue) MenuePunkt = 0; else MenuePunkt++;}
  if((RemoteKeys & KEY1) && (RemoteKeys & KEY2)) MenuePunkt = 0;
  LcdClear();
  if(MenuePunkt < 10) {LCD_printfxy(17,0,"[%i]",MenuePunkt);}
  else {LCD_printfxy(16,0,"[%i]",MenuePunkt);};


  switch(MenuePunkt)
   {
    case 0:
           LCD_printfxy(0,0,"+ MikroKopter +");
           LCD_printfxy(0,1,"HW:V%d.%d SW:%d.%d%c",PlatinenVersion/10,PlatinenVersion%10, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH +'a');
           LCD_printfxy(0,2,"Setting:%d %s",GetActiveParamSetNumber(),Mixer.Name);
           if(I2CTimeout < 6) LCD_printfxy(0,3,"I2C ERROR!!!")
		   else
           if(MissingMotor) LCD_printfxy(0,3,"Missing BL-Ctrl:%d!!",MissingMotor)
           else LCD_printfxy(0,3,"(c) Holger Buss");
//           if(RemoteTasten & KEY3) TestInt--;
//           if(RemoteTasten & KEY4) TestInt++;
           break;
    case 1:
          if(EE_Parameter.GlobalConfig & CFG_HOEHENREGELUNG)
           {
           LCD_printfxy(0,0,"Hoehe:     %5i",HoehenWert);
           LCD_printfxy(0,1,"SollHoehe: %5i",SollHoehe);
           LCD_printfxy(0,2,"Luftdruck: %5i",MessLuftdruck);
           LCD_printfxy(0,3,"Off      : %5i",DruckOffsetSetting);
           }
           else
           {
           LCD_printfxy(0,1,"Keine ");
           LCD_printfxy(0,2,"Höhenregelung");
           }

           break;
    case 2:
           LCD_printfxy(0,0,"akt. Lage");
           LCD_printfxy(0,1,"Nick:      %5i",IntegralNick/1024);
           LCD_printfxy(0,2,"Roll:      %5i",IntegralRoll/1024);
           LCD_printfxy(0,3,"Kompass:   %5i",KompassValue);
           break;
    case 3:
           LCD_printfxy(0,0,"K1:%4i  K2:%4i ",PPM_in[1],PPM_in[2]);
           LCD_printfxy(0,1,"K3:%4i  K4:%4i ",PPM_in[3],PPM_in[4]);
           LCD_printfxy(0,2,"K5:%4i  K6:%4i ",PPM_in[5],PPM_in[6]);
           LCD_printfxy(0,3,"K7:%4i  K8:%4i ",PPM_in[7],PPM_in[8]);
           break;
    case 4:
           LCD_printfxy(0,0,"Ni:%4i  Ro:%4i ",PPM_in[EE_Parameter.Kanalbelegung[K_NICK]],PPM_in[EE_Parameter.Kanalbelegung[K_ROLL]]);
           LCD_printfxy(0,1,"Gs:%4i  Gi:%4i ",PPM_in[EE_Parameter.Kanalbelegung[K_GAS]],PPM_in[EE_Parameter.Kanalbelegung[K_GIER]]);
           LCD_printfxy(0,2,"P1:%4i  P2:%4i ",PPM_in[EE_Parameter.Kanalbelegung[K_POTI1]],PPM_in[EE_Parameter.Kanalbelegung[K_POTI2]]);
           LCD_printfxy(0,3,"P3:%4i  P4:%4i ",PPM_in[EE_Parameter.Kanalbelegung[K_POTI3]],PPM_in[EE_Parameter.Kanalbelegung[K_POTI4]]);
           break;
    case 5:
           LCD_printfxy(0,0,"Gyro - Sensor");
          if(PlatinenVersion == 10)
          {
           LCD_printfxy(0,1,"Nick %4i (%3i.%i)",AdWertNick - AdNeutralNick/8, AdNeutralNick/8, AdNeutralNick%8);
           LCD_printfxy(0,2,"Roll %4i (%3i.%i)",AdWertRoll - AdNeutralRoll/8, AdNeutralRoll/8, AdNeutralRoll%8);
           LCD_printfxy(0,3,"Gier %4i (%3i)",AdNeutralGier - AdWertGier, AdNeutralGier);
          }
          else
          if((PlatinenVersion == 11) || (PlatinenVersion == 20))
          {
           LCD_printfxy(0,1,"Nick %4i (%3i.%x)",AdWertNick - AdNeutralNick/8, AdNeutralNick/16, (AdNeutralNick%16)/2);
           LCD_printfxy(0,2,"Roll %4i (%3i.%x)",AdWertRoll - AdNeutralRoll/8, AdNeutralRoll/16, (AdNeutralRoll%16)/2);
           LCD_printfxy(0,3,"Gier %4i (%3i)",AdNeutralGier - AdWertGier, AdNeutralGier/2);
          }
          else
          if(PlatinenVersion == 13)
          {
           LCD_printfxy(0,1,"Nick %4i (%3i)(%3i)",AdWertNick - AdNeutralNick/8, AdNeutralNick/16,AnalogOffsetNick);
           LCD_printfxy(0,2,"Roll %4i (%3i)(%3i)",AdWertRoll - AdNeutralRoll/8, AdNeutralRoll/16,AnalogOffsetRoll);
           LCD_printfxy(0,3,"Gier %4i (%3i)(%3i)",AdNeutralGier - AdWertGier, AdNeutralGier/2,AnalogOffsetGier);
          }

           break;
    case 6:
           LCD_printfxy(0,0,"ACC - Sensor");
           LCD_printfxy(0,1,"Nick %4i (%3i)",AdWertAccNick,NeutralAccX);
           LCD_printfxy(0,2,"Roll %4i (%3i)",AdWertAccRoll,NeutralAccY);
           LCD_printfxy(0,3,"Hoch %4i (%3i)",Mittelwert_AccHoch/*accumulate_AccHoch / messanzahl_AccHoch*/,(int)NeutralAccZ);
           break;
    case 7:
           LCD_printfxy(0,1,"Spannung:  %5i",UBat);
           LCD_printfxy(0,2,"Empf.Pegel:%5i",SenderOkay);
           break;
    case 8:
           LCD_printfxy(0,0,"Kompass       ");
           LCD_printfxy(0,1,"Richtung:  %5i",KompassRichtung);
           LCD_printfxy(0,2,"Messwert:  %5i",KompassValue);
           LCD_printfxy(0,3,"Start:     %5i",KompassStartwert);
           break;
    case 9:
           LCD_printfxy(0,0,"Poti1:  %3i",Poti1);
           LCD_printfxy(0,1,"Poti2:  %3i",Poti2);
           LCD_printfxy(0,2,"Poti3:  %3i",Poti3);
           LCD_printfxy(0,3,"Poti4:  %3i",Poti4);
           break;
    case 10:
           LCD_printfxy(0,0,"Servo  " );
           LCD_printfxy(0,1,"Setpoint  %3i",Parameter_ServoNickControl);
           LCD_printfxy(0,2,"Stellung: %3i",ServoValue);
           LCD_printfxy(0,3,"Range:%3i-%3i",EE_Parameter.ServoNickMin,EE_Parameter.ServoNickMax);
           break;
    case 11:
           LCD_printfxy(0,0,"ExternControl  " );
           LCD_printfxy(0,1,"Ni:%4i  Ro:%4i ",ExternControl.Nick,ExternControl.Roll);
           LCD_printfxy(0,2,"Gs:%4i  Gi:%4i ",ExternControl.Gas,ExternControl.Gier);
           LCD_printfxy(0,3,"Hi:%4i  Cf:%4i ",ExternControl.Hight,ExternControl.Config);
           break;
    case 12:
           LCD_printfxy(0,0,"BL-Ctrl Errors " );
           LCD_printfxy(0,1," %3d  %3d  %3d  %3d ",MotorError[0],MotorError[1],MotorError[2],MotorError[3]);
           LCD_printfxy(0,2," %3d  %3d  %3d  %3d ",MotorError[4],MotorError[5],MotorError[6],MotorError[7]);
           LCD_printfxy(0,3," %3d  %3d  %3d  %3d ",MotorError[8],MotorError[9],MotorError[10],MotorError[11]);
           break;
    case 13:
           LCD_printfxy(0,0,"BL-Ctrl found " );
           LCD_printfxy(0,1," %c   %c   %c   %c ",MotorPresent[0] + '-',MotorPresent[1] + '-',MotorPresent[2] + '-',MotorPresent[3] + '-');
           LCD_printfxy(0,2," %c   %c   %c   %c ",MotorPresent[4] + '-',MotorPresent[5] + '-',MotorPresent[6] + '-',MotorPresent[7] + '-');
           LCD_printfxy(0,3," %c   -   -   -",MotorPresent[8] + '-');
		   if(MotorPresent[9]) LCD_printfxy(4,3,"10"); 
		   if(MotorPresent[10]) LCD_printfxy(8,3,"11"); 
		   if(MotorPresent[11]) LCD_printfxy(12,3,"12"); 
           break;
    default: MaxMenue = MenuePunkt - 1;
             MenuePunkt = 0;
           break;
    }
    RemoteKeys = 0;
}
