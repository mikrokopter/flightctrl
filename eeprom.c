// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Konstanten 
// + 0-250 -> normale Werte
// + 251 -> Poti1
// + 252 -> Poti2
// + 253 -> Poti3
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void DefaultStickMapping(void)
{
 EE_Parameter.Kanalbelegung[K_NICK]  = 1;
 EE_Parameter.Kanalbelegung[K_ROLL]  = 2;
 EE_Parameter.Kanalbelegung[K_GAS]   = 3;
 EE_Parameter.Kanalbelegung[K_GIER]  = 4;
 EE_Parameter.Kanalbelegung[K_POTI1] = 5;
 EE_Parameter.Kanalbelegung[K_POTI2] = 6;
 EE_Parameter.Kanalbelegung[K_POTI3] = 7;
 EE_Parameter.Kanalbelegung[K_POTI4] = 8;
}

void DefaultKonstanten1(void)
{
 EE_Parameter.GlobalConfig = CFG_ACHSENKOPPLUNG_AKTIV | CFG_KOMPASS_AKTIV | CFG_GPS_AKTIV;//CFG_HOEHEN_SCHALTER 
 EE_Parameter.Hoehe_MinGas = 30;
 EE_Parameter.MaxHoehe     = 251;      // Wert : 0-250   251 -> Poti1
 EE_Parameter.Hoehe_P      = 10;       // Wert : 0-32
 EE_Parameter.Luftdruck_D  = 30;       // Wert : 0-250
 EE_Parameter.Hoehe_ACC_Wirkung = 30;  // Wert : 0-250
 EE_Parameter.Hoehe_Verstaerkung = 4;  // Wert : 0-50
 EE_Parameter.Stick_P = 15;            // Wert : 1-6
 EE_Parameter.Stick_D = 30;            // Wert : 0-64
 EE_Parameter.Gier_P = 12;             // Wert : 1-20
 EE_Parameter.Gas_Min = 8;             // Wert : 0-32
 EE_Parameter.Gas_Max = 230;           // Wert : 33-250
 EE_Parameter.GyroAccFaktor = 30;      // Wert : 1-64
 EE_Parameter.KompassWirkung = 128;    // Wert : 0-250
 EE_Parameter.Gyro_P = 80;             // Wert : 0-250
 EE_Parameter.Gyro_I = 150;            // Wert : 0-250
 EE_Parameter.Gyro_D = 3;              // Wert : 0-250
 EE_Parameter.UnterspannungsWarnung = 94; // Wert : 0-250
 EE_Parameter.NotGas = 35;                // Wert : 0-250     // Gaswert bei Empangsverlust
 EE_Parameter.NotGasZeit = 30;            // Wert : 0-250     // Zeit bis auf NotGas geschaltet wird, wg. Rx-Problemen
 EE_Parameter.UfoAusrichtung = 0;         // X oder + Formation
 EE_Parameter.I_Faktor = 32;
 EE_Parameter.UserParam1 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam2 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam3 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam4 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam5 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam6 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam7 = 0;             // zur freien Verwendung
 EE_Parameter.UserParam8 = 0;             // zur freien Verwendung
 EE_Parameter.ServoNickControl = 100;     // Wert : 0-250     // Stellung des Servos
 EE_Parameter.ServoNickComp = 40;         // Wert : 0-250     // Einfluss Gyro/Servo
 EE_Parameter.ServoNickCompInvert = 0;    // Wert : 0-250     // Richtung Einfluss Gyro/Servo
 EE_Parameter.ServoNickMin = 50;          // Wert : 0-250     // Anschlag
 EE_Parameter.ServoNickMax = 150;         // Wert : 0-250     // Anschlag
 EE_Parameter.ServoNickRefresh = 5;
 EE_Parameter.LoopGasLimit = 50;
 EE_Parameter.LoopThreshold = 90;         // Wert: 0-250  Schwelle f�r Stickausschlag
 EE_Parameter.LoopHysterese = 50;
 EE_Parameter.BitConfig = 0;             // Bitcodiert: 0x01=oben, 0x02=unten, 0x04=links, 0x08=rechts / wird getrennt behandelt
 EE_Parameter.AchsKopplung1 = 90;
 EE_Parameter.AchsKopplung2 = 67;
 EE_Parameter.CouplingYawCorrection = 0;
 EE_Parameter.WinkelUmschlagNick = 85;
 EE_Parameter.WinkelUmschlagRoll = 85;
 EE_Parameter.GyroAccAbgleich = 16;        // 1/k 
 EE_Parameter.Driftkomp = 32;              
 EE_Parameter.DynamicStability = 100;
 EE_Parameter.J16Bitmask = 95; 
 EE_Parameter.J17Bitmask = 243; 
 EE_Parameter.J16Timing = 15; 
 EE_Parameter.J17Timing = 15; 
 EE_Parameter.NaviGpsModeControl = 253;    
 EE_Parameter.NaviGpsGain = 100;     
 EE_Parameter.NaviGpsP = 90;        
 EE_Parameter.NaviGpsI = 90;        
 EE_Parameter.NaviGpsD = 90;        
 EE_Parameter.NaviGpsPLimit = 75;        
 EE_Parameter.NaviGpsILimit = 75;        
 EE_Parameter.NaviGpsDLimit = 75;        
 EE_Parameter.NaviGpsACC = 0;        
 EE_Parameter.NaviGpsMinSat = 6;        
 EE_Parameter.NaviStickThreshold = 8;
 EE_Parameter.NaviWindCorrection = 90;        
 EE_Parameter.NaviSpeedCompensation = 30;        
 EE_Parameter.NaviOperatingRadius = 100;        
 EE_Parameter.NaviAngleLimitation = 100;
 EE_Parameter.NaviPH_LoginTime = 4;
 memcpy(EE_Parameter.Name, "Sport\0", 12);   
}
void DefaultKonstanten2(void)
{
 EE_Parameter.GlobalConfig = CFG_ACHSENKOPPLUNG_AKTIV | CFG_KOMPASS_AKTIV | CFG_GPS_AKTIV;///*CFG_HOEHEN_SCHALTER |*/ CFG_KOMPASS_AKTIV;//0x01;    
 EE_Parameter.Hoehe_MinGas = 30;
 EE_Parameter.MaxHoehe     = 251;         // Wert : 0-250   251 -> Poti1
 EE_Parameter.Hoehe_P      = 10;          // Wert : 0-32
 EE_Parameter.Luftdruck_D  = 30;          // Wert : 0-250
 EE_Parameter.Hoehe_ACC_Wirkung = 30;     // Wert : 0-250
 EE_Parameter.Hoehe_Verstaerkung = 3;     // Wert : 0-50
 EE_Parameter.Stick_P = 12;               // Wert : 1-6
 EE_Parameter.Stick_D = 16;               // Wert : 0-64
 EE_Parameter.Gier_P = 6;                 // Wert : 1-20
 EE_Parameter.Gas_Min = 8;                // Wert : 0-32
 EE_Parameter.Gas_Max = 230;              // Wert : 33-250
 EE_Parameter.GyroAccFaktor = 30;         // Wert : 1-64
 EE_Parameter.KompassWirkung = 128;       // Wert : 0-250
 EE_Parameter.Gyro_P = 80;                // Wert : 0-250
 EE_Parameter.Gyro_I = 120;               // Wert : 0-250
 EE_Parameter.Gyro_D = 3;                 // Wert : 0-250
 EE_Parameter.UnterspannungsWarnung = 94; // Wert : 0-250
 EE_Parameter.NotGas = 35;                // Wert : 0-250     // Gaswert bei Empangsverlust
 EE_Parameter.NotGasZeit = 30;            // Wert : 0-250     // Zeit bis auf NotGas geschaltet wird, wg. Rx-Problemen
 EE_Parameter.UfoAusrichtung = 0;         // X oder + Formation
 EE_Parameter.I_Faktor = 32;
 EE_Parameter.UserParam1 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam2 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam3 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam4 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam5 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam6 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam7 = 0;             // zur freien Verwendung
 EE_Parameter.UserParam8 = 0;             // zur freien Verwendung
 EE_Parameter.ServoNickControl = 100;     // Wert : 0-250     // Stellung des Servos
 EE_Parameter.ServoNickComp = 40;         // Wert : 0-250     // Einfluss Gyro/Servo
 EE_Parameter.ServoNickCompInvert = 0;    // Wert : 0-250     // Richtung Einfluss Gyro/Servo
 EE_Parameter.ServoNickMin = 50;          // Wert : 0-250     // Anschlag
 EE_Parameter.ServoNickMax = 150;         // Wert : 0-250     // Anschlag
 EE_Parameter.ServoNickRefresh = 5;
 EE_Parameter.LoopGasLimit = 50;
 EE_Parameter.LoopThreshold = 90;         // Wert: 0-250  Schwelle f�r Stickausschlag
 EE_Parameter.LoopHysterese = 50;
 EE_Parameter.BitConfig = 0;             // Bitcodiert: 0x01=oben, 0x02=unten, 0x04=links, 0x08=rechts 
 EE_Parameter.AchsKopplung1 = 90;
 EE_Parameter.AchsKopplung2 = 67;
 EE_Parameter.CouplingYawCorrection = 60;
 EE_Parameter.WinkelUmschlagNick = 85;
 EE_Parameter.WinkelUmschlagRoll = 85;
 EE_Parameter.GyroAccAbgleich = 32;        // 1/k 
 EE_Parameter.Driftkomp = 32;              
 EE_Parameter.DynamicStability = 75;
 EE_Parameter.J16Bitmask = 95; 
 EE_Parameter.J17Bitmask = 243; 
 EE_Parameter.J16Timing = 20; 
 EE_Parameter.J17Timing = 20; 
 EE_Parameter.NaviGpsModeControl = 253;    
 EE_Parameter.NaviGpsGain = 100;     
 EE_Parameter.NaviGpsP = 90;        
 EE_Parameter.NaviGpsI = 90;        
 EE_Parameter.NaviGpsD = 90;        
 EE_Parameter.NaviGpsPLimit = 75;        
 EE_Parameter.NaviGpsILimit = 75;        
 EE_Parameter.NaviGpsDLimit = 75;        
 EE_Parameter.NaviGpsACC = 0;        
 EE_Parameter.NaviGpsMinSat = 6;        
 EE_Parameter.NaviStickThreshold = 8;
 EE_Parameter.NaviWindCorrection = 90;        
 EE_Parameter.NaviSpeedCompensation = 30;        
 EE_Parameter.NaviOperatingRadius = 100;        
 EE_Parameter.NaviAngleLimitation = 100;
 EE_Parameter.NaviPH_LoginTime = 4;
 memcpy(EE_Parameter.Name, "Normal\0", 12);   
}

void DefaultKonstanten3(void)
{
 EE_Parameter.GlobalConfig = CFG_DREHRATEN_BEGRENZER | CFG_ACHSENKOPPLUNG_AKTIV | CFG_KOMPASS_AKTIV | CFG_GPS_AKTIV;///*CFG_HOEHEN_SCHALTER |*/ CFG_KOMPASS_AKTIV;//0x01;    
 EE_Parameter.Hoehe_MinGas = 30;
 EE_Parameter.MaxHoehe     = 251;         // Wert : 0-250   251 -> Poti1
 EE_Parameter.Hoehe_P      = 10;          // Wert : 0-32
 EE_Parameter.Luftdruck_D  = 30;          // Wert : 0-250
 EE_Parameter.Hoehe_ACC_Wirkung = 30;     // Wert : 0-250
 EE_Parameter.Hoehe_Verstaerkung = 3;     // Wert : 0-50
 EE_Parameter.Stick_P = 8;                // Wert : 1-6
 EE_Parameter.Stick_D = 16;               // Wert : 0-64
 EE_Parameter.Gier_P  = 6;                // Wert : 1-20
 EE_Parameter.Gas_Min = 8;                // Wert : 0-32
 EE_Parameter.Gas_Max = 230;              // Wert : 33-250
 EE_Parameter.GyroAccFaktor = 30;         // Wert : 1-64
 EE_Parameter.KompassWirkung = 128;       // Wert : 0-250
 EE_Parameter.Gyro_P = 100;               // Wert : 0-250
 EE_Parameter.Gyro_I = 120;               // Wert : 0-250
 EE_Parameter.Gyro_D = 3;                 // Wert : 0-250
 EE_Parameter.UnterspannungsWarnung = 94; // Wert : 0-250
 EE_Parameter.NotGas = 35;                // Wert : 0-250     // Gaswert bei Empangsverlust
 EE_Parameter.NotGasZeit = 20;            // Wert : 0-250     // Zeit bis auf NotGas geschaltet wird, wg. Rx-Problemen
 EE_Parameter.UfoAusrichtung = 0;         // X oder + Formation
 EE_Parameter.I_Faktor = 16;
 EE_Parameter.UserParam1 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam2 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam3 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam4 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam5 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam6 =   0;           // zur freien Verwendung
 EE_Parameter.UserParam7 = 0;             // zur freien Verwendung
 EE_Parameter.UserParam8 = 0;             // zur freien Verwendung
 EE_Parameter.ServoNickControl = 100;     // Wert : 0-250     // Stellung des Servos
 EE_Parameter.ServoNickComp = 40;         // Wert : 0-250     // Einfluss Gyro/Servo
 EE_Parameter.ServoNickCompInvert = 0;    // Wert : 0-250     // Richtung Einfluss Gyro/Servo
 EE_Parameter.ServoNickMin = 50;          // Wert : 0-250     // Anschlag
 EE_Parameter.ServoNickMax = 150;         // Wert : 0-250     // Anschlag
 EE_Parameter.ServoNickRefresh = 5;
 EE_Parameter.LoopGasLimit = 50;
 EE_Parameter.LoopThreshold = 90;         // Wert: 0-250  Schwelle f�r Stickausschlag
 EE_Parameter.LoopHysterese = 50;
 EE_Parameter.BitConfig = 0;             // Bitcodiert: 0x01=oben, 0x02=unten, 0x04=links, 0x08=rechts 
 EE_Parameter.AchsKopplung1 = 90;
 EE_Parameter.AchsKopplung2 = 67;
 EE_Parameter.CouplingYawCorrection = 70;
 EE_Parameter.WinkelUmschlagNick = 85;
 EE_Parameter.WinkelUmschlagRoll = 85;
 EE_Parameter.GyroAccAbgleich = 32;        // 1/k 
 EE_Parameter.Driftkomp = 32;              
 EE_Parameter.DynamicStability = 50;
 EE_Parameter.J16Bitmask = 95; 
 EE_Parameter.J17Bitmask = 243; 
 EE_Parameter.J16Timing = 30; 
 EE_Parameter.J17Timing = 30; 
 EE_Parameter.NaviGpsModeControl = 253;    
 EE_Parameter.NaviGpsGain = 100;     
 EE_Parameter.NaviGpsP = 90;        
 EE_Parameter.NaviGpsI = 90;        
 EE_Parameter.NaviGpsD = 90;        
 EE_Parameter.NaviGpsPLimit = 75;        
 EE_Parameter.NaviGpsILimit = 75;        
 EE_Parameter.NaviGpsDLimit = 75;        
 EE_Parameter.NaviGpsACC = 0;        
 EE_Parameter.NaviGpsMinSat = 6;        
 EE_Parameter.NaviStickThreshold = 8;
 EE_Parameter.NaviWindCorrection = 90;        
 EE_Parameter.NaviSpeedCompensation = 30;        
 EE_Parameter.NaviOperatingRadius = 100;        
 EE_Parameter.NaviAngleLimitation = 100;
 EE_Parameter.NaviPH_LoginTime = 4;
 memcpy(EE_Parameter.Name, "Beginner\0", 12);   
}
